describe('Users factory', function() {
  var Users;
  beforeEach(angular.mock.module('pupils'));

  beforeEach(inject(function(_Users_) {
    Users = _Users_;
  }));

  it('should exist', function() {
    expect(Users).toBeDefined();
  });

  describe('.all()', function() {
    it('should exist', function() {
      expect(Users.all).toBeDefined();
    });

    it('should return a hard-coded list of users', function() {
      expect(Users.all().length).toBeGreaterThan(0);
    });
  });

  describe('.findById()', function() {
    it('should exist', function() {
      expect(Users.findById).toBeDefined();
    });
    it('should return undefined if the user cannot be found', function() {
      expect(Users.findById('ABC')).not.toBeDefined();
    });
  });

  describe('.add()', function(){
    it('should sort the list alphabetically by name if new users are inserted', function () {
      Users.clear();

      expect(Users.all().length).toBe(0);

      var markus = Users.add({
        id: 20,
        name: 'Markus',
        location: 'Linz',
        role: 'developer'
      });

      var alfonso = Users.add({
        id: 32,
        name: 'Alfonso',
        location: 'Hart',
        role: 'SQL-Profi'
      });

      expect(Users.all().length).toBe(2);
      expect(Users.all()[0]).toBe(alfonso);
      expect(Users.all()[1]).toBe(markus);
    });
  });
});
