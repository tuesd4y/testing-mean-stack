'use strict';

//browser.sleep(2000);

describe('my app', function() {

  it('should have the correct title', function() {
    browser.get('/');
    expect(browser.getTitle()).toMatch("User List");
  });

  it('should have the correct headline', function () {
    browser.get('/');

    var header = element(by.css('.header'));
    expect(header.getText()).toBe('User List');
  });
  it('should have only active users', function () {
    browser.get('/');

    var activeUsers = element.all(by.css('.active li'));
    expect(activeUsers.count()).toBe(4);

    var inactiveUsers = element.all(by.css('.inactive li'));
    expect(inactiveUsers.count()).toBe(0);

  });

  it('should be able to remove active users', function(){
    browser.get('/');

    var activeUsers = element.all(by.css('.active li'));
    expect(activeUsers.count()).toBe(4);

    var inactiveUsers = element.all(by.css('.inactive li'));
    expect(inactiveUsers.count()).toBe(0);

    var firstActiveUserText = activeUsers.get(0).getText().then(function(text){
      return text.split(' ')[0]
    });
    activeUsers.get(0).element(by.css('button')).click();

    expect(activeUsers.count()).toBe(3);
    expect(inactiveUsers.count()).toBe(1);

    expect(inactiveUsers.get(0).getText().then(function(text){
      return text.split(' ')[0]
    })).toBe(firstActiveUserText);
  });

  it('should be able to reactivate users', function(){
    browser.get('/');

    var activeUsers = element.all(by.css('.active li'));
    expect(activeUsers.count()).toBe(4);

    var inactiveUsers = element.all(by.css('.inactive li'));
    expect(inactiveUsers.count()).toBe(0);

    var firstActiveUserText = activeUsers.get(0).getText();
    activeUsers.get(0).element(by.css('button')).click();

    expect(activeUsers.count()).toBe(3);
    expect(inactiveUsers.count()).toBe(1);

    inactiveUsers.get(0).$('button').click();


    expect(activeUsers.count()).toBe(4);
    expect(inactiveUsers.count()).toBe(0);

    expect(activeUsers.get(0).getText()).toEqual(firstActiveUserText);
  })
});
