# Testing MEAN-Stack projects

### Run regular project

`npm install`

and then 

`npm start`

### Run tests

#### Unit tests (karma + jasmine)

with file watcher: `npm test`

single run: `npm run test-single`

#### e2e tess (protractor)

since protractor depends on the selenium webdriver you need to have the jdk installed

run `npm run protractor-prepare` once

then run tests with `npm run protractor`

_by Christopher Stelzmüller_

[Presentation](https://slides.com/tuesd4y/mean-testing)

## Sources

https://qunitjs.com

https://jasmine.github.io

https://mochajs.org

https://karma-runner.github.io

http://www.protractortest.org

[http://techtalkdc.com](http://www.techtalkdc.com/which-javascript-test-library-should-you-use-qunit-vs-jasmine-vs-mocha/)

[http://scotch.io](https://scotch.io/tutorials/testing-angularjs-with-jasmine-and-karma-part-1)

[http://mherman.org](http://mherman.org/blog/2015/04/09/testing-angularjs-with-protractor-and-karma-part-1/#.WDtqZp-qZhE)