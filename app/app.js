(function() {
  'use strict';

  angular.module('pupils', [
    'ui.router'
  ])
    .config(function($urlRouterProvider) {
    $urlRouterProvider.otherwise("/");
  })
    .factory('Users', function() {
      var Users = {};
      var userList = [
        {
          id: '1',
          name: 'Jane',
          role: 'Designer',
          location: 'New York'
        },
        {
          id: '2',
          name: 'Bob',
          role: 'Developer',
          location: 'New York'
        },
        {
          id: '3',
          name: 'Jim',
          role: 'Developer',
          location: 'Chicago'
        },
        {
          id: '4',
          name: 'Bill',
          role: 'Designer',
          location: 'LA'
        }
      ];

      var sort = function(){
        userList.sort(function(item1, item2) {
          return item1.name > item2.name;
        });
      };

      Users.all = function() {
        sort();
        return userList;
      };

      Users.findById = function(id) {
        return userList.find(function(user) {
          return user.id === id;
        });
      };

      Users.count = function() {
        return userList.length;
      };

      Users.delete = function(user) {
        for(var i = 0; i < userList.length; i++){
          if(user.id === userList[i].id){
            userList.splice(i, 1);
            return true;
          }
        }
        return false;
      };

      Users.add = function (user) {
        for(var i = 0; i < userList.length; i++){
          if(user.id === userList[i].id){
            return false;
          }
        }
        userList.push(user);
        return user;
      };

      Users.sort = function () {
        sort();
        return Users.all();
      };

      Users.sortBy = function (fn) {
        return userList.sort(fn)
      };

      Users.clear = function(){
        userList = [];
      };

      Users.sort();

      return Users;
    })
    .controller('PupilController', ['$scope', 'Users', function ($scope, Users) {
      $scope.title = "User List";
      $scope.users = Users.all();
      $scope.inactiveUsers = [];

      $scope.deactivate = function (user) {
        Users.delete(user);
        Users.sort();
        $scope.users = Users.all();
        $scope.inactiveUsers.push(user);
      };
      $scope.activate = function (user) {
        Users.add(user);
        Users.sort();
        $scope.users = Users.all();
        for(var i = 0; i < $scope.inactiveUsers.length; i++){
          if(user.id === $scope.inactiveUsers[i].id){
            $scope.inactiveUsers.splice(i, 1);
          }
        }
      };
    }])
})();
